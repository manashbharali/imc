@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">

                <div class="card-header">
                    <strong>Garbage Bin Create</strong>
                </div>

                <!--card-header-->
                <div class="card-body">
                    <div class="pb-3">
                        <form action="{{ route('admin.garbage.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="" class="form-control-label">Location</label>
                                <input type="text" class="form-control @error('location') is-invalid @enderror"
                                    placeholder="Address" name="location">
                                @error('location')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="" class="form-control-label">Lat</label>
                                <input type="text" class="form-control @error('lat') is-invalid @enderror" placeholder="Address" name="lat">
                                @error('lat')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="" class="form-control-label @error('long') is-invalid @enderror">Long</label>
                                <input type="text" class="form-control" placeholder="Address" name="long">
                                @error('long')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div>
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--card-body-->
            </div>
            <!--card-->

        </div>
        <!--col-->
    </div>
    <!--row-->
@endsection
