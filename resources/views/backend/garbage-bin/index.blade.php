@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>Garbage Bin Dashboard</strong>
                </div>
                <!--card-header-->
                <div class="card-body">
                    <div class="pb-3">
                        <a href="{{ route('admin.garbage.create')}}" class="btn btn-primary">Create</a>
                    </div>
                    <table class="table dataTable no-footer">
                        <tr>
                            <th>ID</th>
                            <th>Location</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($garbage_bin as $garbage)
						<tr>
							<td>{{ $garbage->id }}</td>
							<td>{{ $garbage->location }}</td>
							<td>{{ $garbage->lat }}</td>
							<td>{{ $garbage->long }}</td>
                            <td>
                                <form action="{{ route('admin.garbage.delete', $garbage->id) }}"
                                    method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="button" class="btn btn-danger btn-sm" onclick="if (confirm('Are you sure?')) { this.form.submit() } ">
                                        Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-->
    </div>
    <!--row-->
@endsection
