@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">

                <div class="card-header">
                    <strong>Garbage Bin Create</strong>
                </div>

                <!--card-header-->
                <div class="card-body">
                    <div class="pb-3">
                        <form action="{{ route('admin.image.store') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="" class="form-control-label">Select Image</label>
                                <input type="file" class="form-control" name="image_name">
                            </div>
							<div>
								<button class="btn btn-primary">Save image</button>
							</div>
                        </form>
                    </div>
                </div>
                <!--card-body-->
            </div>
            <!--card-->

        </div>
        <!--col-->
    </div>
    <!--row-->
@endsection
