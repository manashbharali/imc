@extends('backend.layouts.app')
@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))
@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>Garbage Bin Dashboard</strong>
                </div>
                <!--card-header-->
                <div class="card-body">
                    <div class="pb-3">
                        <a href="{{ route('admin.image.create') }}" class="btn btn-primary">Create</a>
                    </div>
                    <table class="table dataTable no-footer">
                        <tr>
                            <th>ID</th>
                            <th>Image Name</th>
                            <th>Created at</th>
                            <th>View</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($images as $img)
						<tr>
							<td>{{ $img->id }}</td>
							<td>{{ $img->image_name }}</td>
							<td>{{ $img->created_at->diffForHumans() }}</td>
                            <td> <a href="{{ asset('storage/image-slider/images')}}/{{$img->image_name}}" class="btn btn-primary btn-sm">View</a> </td>
							<td>
                                <form action="{{ route('admin.image.delete', $img->id) }}"
                                    method="post">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="button" class="btn btn-danger btn-sm" onclick="if (confirm('Are you sure?')) { this.form.submit() } ">
                                        Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <!--card-body-->
            </div>
            <!--card-->
        </div>
        <!--col-->
    </div>
    <!--row-->
@endsection
