<?php

/**
 * All route names are prefixed with 'admin.'.
*/

Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::post('get-permission', 'DashboardController@getPermissionByRole')->name('get.permission');

Route::get('garbage-bin', 'GarbageBinController@index')->name('garbage.index');
Route::get('garbage-bin/create', 'GarbageBinController@create')->name('garbage.create');
Route::post('garbage-bin/store', 'GarbageBinController@store')->name('garbage.store');
Route::delete('garbage-bin/{id}', 'GarbageBinController@destroy')->name('garbage.delete');

Route::get('img-slider', 'ImageSliderController@index')->name('image.index');
Route::get('img-slider/create', 'ImageSliderController@create')->name('image.create');
Route::post('img-slider/store', 'ImageSliderController@store')->name('image.store');
Route::delete('img-slider/{id}', 'ImageSliderController@destroy')->name('image.delete');

/*
Route::get('profile/edit', 'DashboardController@editProfile')->name('profile.edit');
Route::patch('profile/update', 'DashboardController@updateProfile')->name('profile.update');
*/
