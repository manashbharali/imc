<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
	$trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.garbage.index', function ($trail) {
	$trail->push(__('Garbage Bin'), route('admin.garbage.index'));
});

Breadcrumbs::for('admin.garbage.create', function ($trail) {
	$trail->push(__('Garbage Bin create'), route('admin.garbage.create'));
});

Breadcrumbs::for('admin.image.index', function ($trail) {
	$trail->push(__('Image Slider'), route('admin.image.index'));
});

Breadcrumbs::for('admin.image.create', function ($trail) {
	$trail->push(__('Image Slider'), route('admin.image.create'));
});

require __DIR__ . '/auth.php';
require __DIR__ . '/log-viewer.php';
require __DIR__ . '/blogs/blog.php';
require __DIR__ . '/blog-categories/blog-categories.php';
require __DIR__ . '/blog-tags/blog-tags.php';
require __DIR__ . '/pages/page.php';
require __DIR__ . '/faqs/faq.php';
require __DIR__ . '/email-templates/email-template.php';
require __DIR__ . '/auth/permission.php';
