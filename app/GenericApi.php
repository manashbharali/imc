<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenericApi extends Model
{
    protected $fillable = ['name', 'url'];
}
