<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComplainRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string|min:200|max:600',
            'address' => 'required|string|max:255',
            // 'images' =>  'required',
            'phone_no' => 'required|numeric|digits:10',
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'device_info' => 'required|max:600',
        ];
    }
}
