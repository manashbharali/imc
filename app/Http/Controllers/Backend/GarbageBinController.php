<?php

namespace App\Http\Controllers\Backend;

use App\GarbageBin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GarbageBinController extends Controller
{
	public function index(){
		$garbage_bin = GarbageBin::all();
		return view('backend.garbage-bin.index')->with('garbage_bin', $garbage_bin);
	}

	public function create(){
		return view('backend.garbage-bin.create');
	}

	public function store(Request $request){
		$request->validate([
			'location' => 'required',
			'lat' => 'required',
			'long' => 'required',
		]);
		GarbageBin::create([
			'location' => $request->location,
			'lat' => $request->lat,
			'long' => $request->long,
		]);
		return redirect()->route('admin.garbage.index')->with('success', 'Saved');
	}

	public function destroy($id)
	{
		GarbageBin::findOrFail($id)->delete();
		return redirect()->route('admin.garbage.index')->with('success', 'Deleted');
	}

}
