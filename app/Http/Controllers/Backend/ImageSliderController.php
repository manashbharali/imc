<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ImageSlider;

class ImageSliderController extends Controller
{
	public function index()
	{
		$images = ImageSlider::all();
		return view('backend.imageSlider.index')->with('images', $images);
	}

	public function create()
	{
		$images = ImageSlider::all();
		return view('backend.imageSlider.create');
	}

	public function store(Request $request)
	{
		$request->validate([
			'image_name' => 'required|image',
		]);

		if ($request->hasFile('image_name')) {
			$filenameWithExt = $request->file('image_name')->getClientOriginalName();
			$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			$extension = $request->file('image_name')->getClientOriginalExtension();
			$fileNameToStore = $filename . '_' . time() . '.' . $extension;
			$path = $request->file('image_name')->storeAs('public/image-slider/images', $fileNameToStore);
		} 
		ImageSlider::create([
			'image_name' => $fileNameToStore,
		]);
		return redirect()->route('admin.image.index')->with('success', 'Saved');
	}

	public function destroy($id)
	{
		ImageSlider::findOrFail($id)->delete();
		return redirect()->route('admin.image.index')->with('success', 'Deleted');
	}
}
