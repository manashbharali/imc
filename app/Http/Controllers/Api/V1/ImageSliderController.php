<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\ImageSlider;
use Illuminate\Http\Request;

class ImageSliderController extends Controller
{
    public function index()
    {
		return ImageSlider::all();
    }
}
