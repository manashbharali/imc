<?php

namespace App\Http\Controllers\Api\V1;

use App\GarbageBin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GarbageBinController extends Controller
{
	
	public function index()
	{
		return GarbageBin::all();
	}

	public function show($id)
	{
		$data = GarbageBin::findOrFail($id)->get();
		return $data;
	}

}
