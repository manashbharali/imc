<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ComplainRequest;
use App\Http\Resources\ComplainResource;
use App\Models\Complaint;
use App\Models\Image;
use Illuminate\Http\Request;

class ComplainController extends Controller
{

    public function index()
    {
        // return ComplainResource::collection(Complaint::paginate(25));
        return ComplainResource::collection(Complaint::all());
    }

    public function store(ComplainRequest $request)
    {
        $request->validated();
        if (!$request->hasFile('fileName')) {
            return response()->json(['upload_file_not_found'], 400);
        }
        $allowedfileExtension = ['pdf', 'jpg', 'png'];
        $files = $request->file('fileName');
        $extension = $files->getClientOriginalExtension();
        $check = in_array($extension, $allowedfileExtension);
        if ($check) {
            $files->store('public/complaints/images');
            $name = $files->getClientOriginalName();
            $complain = Complaint::create($request->all());
            $save = new Image();
            $save->img_name = $name;
            $save->complain_id = $complain->id;
            $save->save();
        }

        return response()->json(['complain registered successfully'], 200);
    }

    public function show($id)
    {
        $complaint = Complaint::findOrFail($id);
        return new ComplainResource($complaint);
    }

    public function update(Request $request, $id)
    {
        // $request->validated();
        $complain = Complaint::findOrFail($id);
        if($complain->update($request->all())){
            return response()->json(['complain updated successfully'], 200);
        }
        return 'Error';
    }

    public function destroy($id)
    {
        Complaint::findOrFail($id)->delete();
        return response()->json("Successfully Deleted!", 200);
    }
}
