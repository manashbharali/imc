<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GenericApi;

class GenericApiController extends Controller
{
    public function index()
    {
        return GenericApi::paginate(25);
    }

    public function show($id)
    {
        $data = GenericApi::findOrFail($id)->get();
        return $data;
    }
}
