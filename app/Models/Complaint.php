<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = ['title', 'description', 'address', 'images', 'phone_no', 'longitude', 'latitude', 'device_info', 'status'];
}
