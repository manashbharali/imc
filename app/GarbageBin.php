<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GarbageBin extends Model
{
    //
    protected $fillable = ['location', 'lat', 'long'];

}
